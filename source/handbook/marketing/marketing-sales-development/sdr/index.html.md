---
layout: markdown_page
title: "Sales Development"
---
---
# Table of Contents
- [Your Role](#role)
- [Inbound BDR Handbook](#inbound)
- [Outbound SDR Handbook](#outbound)

# Welcome<a name="welcome"></a>

Welcome to GitLab and congratulations on landing a job with one of the best software startups in the world! We are excited to have you join the team and look forward to working closely with you and seeing you grow and develop your career here at GitLab.

As a BDR (Business Development Representative) or SDR (Sales Development Representative) your focus will be on generating opportunities that the AEs (Account Executives) and SALs (Strategic Account Leaders) accept, ultimatley leading to closed won business. On this team we work hard, but have fun too (I know, it's a cliche...but here it's true!). We will work hard to guarantee your success if you do the same. We value results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, solutions, and quirkiness.

Being a BDR or SDR can come with what seems like long days, hard work, frustration, and even at times, failure. If you have the right mindset and come to work with a tenacious, hungry attitude you will see growth personally and in your career. GitLab is a place with endless opportunity. Let’s make it happen!

### Your Role<a name="role"></a>
As a BDR or SDR, you will be dealing with the front end of the sales process. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating sales accepted opportunities for GitLab.

As you gain knowledge, you will be able to aid our future customers ship better software, faster. There are numerous resources at your fingertips that we have created to help you in this process. You have:

1. [BDR Handbook](https://about.gitlab.com/handbook/marketing/business-development/) - This will help you with your day to day workflow. You can find information on how to prospect, best practices, customer FAQs, buyer types, cadence samples and more. Use the SDR handbook in conjunction with the [Marketing](https://about.gitlab.com/handbook/marketing/) and [Sales](https://about.gitlab.com/handbook/sales/) handbooks. This will help you bridge the gap between the two and learn the product and process faster.
2. [GLU GitLab University](https://docs.gitlab.com/ce/university/) - These trainings will teach the fundamentals of Version Control with Git and GitLab through courses that cover topics which can be mastered in about 2 hours. These trainings include an intro to Git, GitLab basics, a demo of Gitlab.com, terminology, and more. You can also find information about GitLab compared to other tools in the market.
3. Gmail
4. Salesforce
5. [Outreach](https://outreach.io/)
6. LinkedIn
7. DiscoverOrg
8. [Grovo](https://www.grovo.com/) - A learning management system with short, engaging tracks focused on specific skills.
9. Slack
10. Google Drive

# Inbound BDR Handbook<a name="inbound"></a>

- [Onboarding](#onboard)

### [Inbound Workflow](#bdring)
- [Lead Definition](#mql)
    - [A Leads](#aLeads)
    - [Passive Leads](#passiveLeads)
- [Lead Management](#leadManagement)
    - [Glossary](/handbook/revenue-ops)
    - [Segmentation](#segmentation)
    - [Lead Routing](#leadRouting)
    - [Initial Source](#initialSource)
    - [Lead/Contact Status](#statuses)
- [Researching](#researching)
- [Prospecting](#prospecting)
   - [Email Prospecting](#emailProspect)
   - [Call Prospecting](#callProspect)
- [Qualifying](#qualifying)
   - [When to Create an Opportunity](#createdopp)
   - [SAO Criteria](#acceptedopp)

### [Miscellaneous](#misc)
- [Variable Compensation Guidelines](#compensation)
- [Additional Resources](#additionalResources)
- [FAQ](#questions)

### Onboarding<a name="onboard"></a>
You will be assigned an onboarding issue by Peopleops. Tasks in the issue will fill up the majority of your first week. This is a step by step guide/checklist to getting everything in your arsenal set up, such as equipment, tools, security, and your Gitlab.com account. These todo’s provide you with the fundamentals.

[Inbound Weekly Onboarding Schedule](https://docs.google.com/a/gitlab.com/document/d/1yHZ0-oZAuq8keAZvFdeXSHdGC_bn3Fdr2VS2EO_Ug64/edit?usp=sharing) - This 3 week schedule will get you up to speed with the basics of BDRing.

### BDR & BDR Team Lead<a name="teamLead"></a>
#### Formal Weekly 1:1
- Mental check-in (winning and success)
- Discuss progress on targeted accounts
- Coaching - email strategy, campaigns, cadence, best practices
- Review goals at the account level and personal level
- Strategy for next week
- Upcoming events/campaigns that can be leveraged
- Personal goals and commitments


### Inbound Workflow<a name="bdring"></a>
- Strategize to develop the proper qualifying questions for all types of prospective customers.
- Be able to identify where a prospective customer is in their buying cycle and take appropriate action to help them along their journey towards becoming a customer.
- Generate Sales Accepted Opportunities (SAOs)
- Inbound to work off of leads within SFDC
- Inbound does not touch any lead that has activity on it within the last 45 days by a different BDR.

### Lead/MQL Definition<a name="mql"></a>
A [Marketo Qualified Lead (MQL)](https://docs.google.com/a/gitlab.com/document/d/1_kSxYGlIZNNyROLda7hieKsrbVtahd-RP6lU6y9gAIk/edit?usp=sharing) is a lead that reaches a certain point threshold (90 pts) based on a demographic, firmographic, and/or behavioral information. Each BDR will be placed into the Marketo queue and will receive a high volume of MQLs to work. Criteria for those leads are set by Marketo and the Marketing Operations team. We have two MQL buckets: (1) hand raisers and (2) passive leads.

### Hand Raisers (A Leads)<a name="aLeads"></a>
Hand raisers are leads who have filled out a [Sales Contact Us](https://about.gitlab.com/sales/) form, signed up for a free [EE trial](https://about.gitlab.com/free-trial/), engaged us through the Web Chat channel, or attended a live Enterprise Edition (EE) Demo. These leads are automatic MQLs regardless of demographic information because they have exhibited clear interest in GitLab’s ability to fulfill a software need.

### Passive Leads<a name="passiveLeads"></a>
Passive leads are leads who have **not** engaged with us in an active manner or completed any of the [Handraiser](#aLeads) actions. For a passive lead to become an MQL, they must meet our minimum person score of 90 pts. A passive lead becomes a MQL via a combination of demographic score and behavior/engagement score.

Because these leads can MQL without showing explicit interest in GitLab paid products, they are routed to Inbound Business Development Representatives to qualify and assess sales-readiness.

### Lead Management<a name="leadManagement"></a>
What leads to qualify, how to do it, and who to send them to. Generally, we follow steps detailed in the [demand waterfall](https://about.gitlab.com/handbook/marketing/marketing-sales-development/#waterfall).

### Segmentation<a name="segmentation"></a>

#### Size
(can be found on the account page in Salesforce)
Strategic = 5000+ users
Large = 751-4999 users
Mid Market = 101-750 users
Small-Medium Business (SMB) = 1-100 users

#### Region/Vertical

##### [APAC](https://docs.google.com/document/d/1Ar0Y49XF0pnvWjhr5-jr0MNKdxfRY2FkS4x9y7KCZw8/edit#)
Asia Pacific
RD = Michael Alessio

##### [EMEA](http://genesisworld.com/assets/uploads/2014/11/map_EMEA.jpg)
Europe, Middle East, and Africa
RD = Richard Pidgeon

##### [US East](https://dev.gitlab.org/gitlab/salesforce/issues/118)
RD = Mark Rogge

##### [US West](https://dev.gitlab.org/gitlab/salesforce/issues/118)
RD = Haydn Mackay

##### Public Sector
Director of Federal Sales = Paul Almeida

### Lead Routing<a name="leadRouting"></a>
#### Contact Requests
Incoming requests that are classified in the `Strategic`, `Large` and `Mid Market` Sales segment, will be routed to the `Strategic Account Leader` or `Account Executive` (aka Account Owner) that owns the account as identified in salesforce.com.
- At the Account Owner's discretion, they may assign lead follow up in these cases to an outbound SDR or inbound BDR or may follow up directly. In any case, follow up must be within **one (1) business day** and tracked as an activity on the record in Salesforce.
- The Account Owner is responsible to ensure communication happens internally and externally within the SLA timeframe.
- The Account Owner is responsible to use the same method of determining `Sales Qualified Amount` so as to not impact our planning in the Revenue Model. This means setting the amount to reflect the number of seats the prospect initially inquiries about, not the amount that will be reflected in the inital order.

All other Contact Requests - `SMB` - will be distributed to the respective Inbound BDR team based on region (EMEA, NCSA, APAC). The contents of the Contact request will determine the follow up next steps:
- Refund: forward the request to AP@. The AP team will handle processing the refund through Zuora & noting the Salesforce record.
- Support: If the inbound BDR can provide a solution to the question please do so. However, if they are a current customer with a Support agreement, it is best to forward to support@.

#### Enterprise Trials (EE Trials)
Free trial requests are routed to the appropriate Inbound BDR team based on region (EMEA, NCSA, APAC). An Enterprise Trial lead is automatically added to a five (5) touch automated nurture in Marketo that runs during the 30-days the trial is active. Additionally the BDR can choose to add the lead to an Outreach sequence that is timed to run in conjunction with the automated Marketo campaign.


### Initial Source<a name="initialSource"></a>  
Initial Source is set upon first "known" touch attribution. It should never be changed or overwritten. If merging leads, keep the Initial Source that was created first (if you can tell). If creating a Lead/Contact and you are unsure what Initial Source to use, ask on #Lead-Questions channel in Slack.
- Advertisement
- AE Generated
- CE Download
- CE Usage Ping
- Clearbit
- Conference
- Consultancy Request
- Contact Request
- Datanyze
- Development Request
- DiscoverOrg
- Email Request
- Email Subscription
- Employee Referral
- Enterprise Trial
- Existing Client
- External Referral
- GitLab.com
- GitLab Hosted
- Gitorious
- Leadware
- Legacy
- LinkedIn
- Live Event
- Newsletter
- Other
- Partner
- Prof Serv Request
- Public Relations
- SDR Generated
- Security Newsletter
- Trade Show
- Training Request
- Web
- Webcast
- Web Chat
- Web Direct
- White Paper
- Word Of Mouth


### Lead & Contact Statuses<a name="statuses"></a>
The Lead & Contact objects in Salesforce have unified statuses with the following definitions. If you have questions about current status, please ask in #Lead-Questions channel on Slack.

| Status | Definition |
| :--- | :--- |
| Raw | Untouched brand new lead |
| Inquiry | Form submission, meeting @ trade show, content offer |
| MQL | Marketo Qualified through systematic means |
| Accepted | Actively working to get intouch with the lead/contact |
| Qualifying | In 2-way conversation with lead/contact |
| Qualified | Progressing to next step of sales funnel (typically OPP created & hand off to Sales team) |
| Unqualified | Contact information is not now or ever valid in future; Spam form fill-out |
| Nurture | Record is not ready for our services or buying conversation now, possibly later |
| Bad Data | Incorrect data - to potentially be researched to find correct data to contact by other means |
| Web Portal Purchase | Used when lead/contact completed a purchase through self-serve channel & duplicate record exists |


### Researching<a name="researching"></a>
#### 3 X 3 X 3
Three things in three minutes about the:
- Organization
- Industry
- Prospect/Lead

### Salesforce
- Search Salesforce.com (SFDC) for the company name.
    - Are there other leads that are being worked?
    - Activity History
        - Who in the organization have we been in contact with? Can we follow up or reference this?
    - Is there an owned customer/prospect account?
    - Are there any open opportunities?

### LinkedIn
- Summary/bio
    - Does anything stand out that is relevant to their needs as an organization?
    - What is the lead’s role and how does that affect your messaging?
    - Have they published any articles that would be worth referencing?
- Previous work
    - Any Gitlab Customers that we can reference?
- Connections
    - Are they connected to anyone at GitLab for a possible introduction?

### Company Website
- Mission Statement
- Press Releases/Newsroom
- (ctrl F) search for keywords

### Prospecting<a name="prospecting"></a>

### Email Prospecting<a name="emailProspect"></a>
- Studies show that Executives read emails in the morning
- Expect to be forwarded to the right person (Always ask)
- Keep emails 90 words or less
- Break into readable stanzas
- Always be specific and tailored
- Make your emails viewable from a mobile device

#### Structuring a Prospecting Email
- Subject line
- Preview pain
- Opening stanza
    - Make it about them, not about you
- Benefit and value proposition
- Call to action (CTA)
    - Offer available times for a meeting/call
    - Create urgency

### Call Prospecting<a name="callProspect"></a>
- Call about them, not about you
- Be confident and passionate
- Aim for every role but focus on technical decision makers
- Ask for time
- Focus on your endgame: Sales Accepted Opportunities (SAO)
- Make it easy to say yes
- Obtain a commitment

### Structuring a Prospecting Call
- Introduction
    - Immediately introduce yourself and GitLab
    - Ask for time
- Initial benefit statement
- Qualification
- Inform them/answer questions about GitLab
- Commitments

### Warm Discovery Calls
Purpose: To qualify leads faster and more accurately (Sales Acceptance Criteria) than in an email exchange.

Process: In your reply message to setup/initiate a call, ask a few of your normal BDR questions to prep for the call. To save a step in emails include your Calendly link so leads may schedule the call directly.

    “Hi (lead name), this is (BDR) from GitLab. How are you today?”

    “Great, is now still a good time to talk about (primary issue)?”

After you’ve established the conversation is good to move forward, ask questions and guide the conversation in a way that enables the lead to tell you what their issue/problem is while also providing answers to the sales acceptance criteria. Your primary role is to gather information, and decide more appropriately how to provide assistance and/or qualify the lead for a call with an AE.

    “Great (lead name), thank you for sharing that information. I have a better feel now for how to move forward with your request/issue. I’m going to follow-up with an email recapping what we discussed, send over that documentation I promised and get you in touch with (account executive)”

### Qualifying<a name="qualifying"></a>
Your goal is to generate Sales Accepted Opportunities (SAOs) by gathering all pertinent information needed for an Account Executive to take action. Some examples of sales qualification questions can be found [here](https://about.gitlab.com/handbook/sales-qualification-questions/).

### SAO Criteria<a name="sql"></a>

#### Current Defined Need
- Does the prospect have an identified need for GitLab?
- What is the prospect currently doing to address their need? What other technologies are they using?
- What version of EE are they interested in?
- [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)

#### Buying Process
- Who is the decision maker for GitLab?
- What is the buying process for procuring GitLab?
- What role does the prospect play in the current evaluation?

#### Product Fit
- Are they interested in GitLab EE? Is GitLab EE a good fit for their need? (Needs to be YES)
- Do they currently use another version of GitLab?
- Are they familiar with our product family?

#### Scope
- How many seats are they interested in purchasing?
- What version of EE are they interested in?
- Are they mid-market, large or strategic in potential size?

#### Next Steps
- Is there a meeting set with an AE to discuss next steps?
- Did they request a quote?
- Are there specific questions/issues the AE should address?

#### When to Create an Opportunity<a name="opportunity"></a>
When you have successfully qualified a lead, you will need to create an opportunity and hand it off to the assigned Account Executive (AE) or Strategic Account Leader (SAL).

#### How to Convert a Lead Into an Opportunity
[This](https://docs.google.com/document/d/1_Q7DIE1tM7IOuCyQXLPFjOBAS52AL7QadpLb7pshw48/edit) document provides a step by step process on how to convert a lead to an opportunity. It includes the following information:
- [Opportunity Naming Conventions](https://about.gitlab.com/handbook/sales/#opportunity-naming-convention)
- Required fields necessary for an opportunity be saved correctly

#### Inbound Handoff Process
Schedule an initial meeting with the Account Owner/AE.
- Schedule the call via Google Calendar, send invites
- Include yourself, the AE, and the prospect on the invite, it is required that you join the call
- Name the event: Gitlab Introductory Meeting - {{Account Name}}
- Insert the AE’s [Zoom meeting link](https://docs.google.com/document/d/1GFpXMjOS-kBsqNV5aFdMho0c4NfU2311MkX-4cogVfQ/edit) into the ‘Where’ section of the Google Calendar invite.

### Miscellaneous<a name="miscellaneous"></a>

### Variable Compensation Guidelines<a name="compensation"></a>
Full-time Inbound BDRs have a significant portion of their pay based on performance and objective attainment. Performance based "bonuses" are based on quota attainment.

Actions for obtaining results will be prescribed and measured, but are intentionally left out of bonus attainment calculations to encourage experimentation. Inbound BDRs will naturally be drawn to activities that have the highest yield, but freedom to choose their own daily activities will allow new, higher yield activities to be discovered.

### Guidelines for Bonuses
- Team and individual quotas are based on GitLab's revenue targets
- Quotas will be made known by having each BDR sign a participant form that clearly lays out quarterly quotas that match the company's revenue plan
- Bonuses are paid quarterly.
- Bonuses are based solely on sales accepted opportunities generated. Guidelines for a [sales accepted opportunity](#acceptedopp)
- A new BDR's first month's bonus is typically based on completing onboarding

### Additional Resources<a name="additionalResources"></a>
- [GitLab Primer](https://about.gitlab.com/primer/)
- [Glossary of Terms](https://docs.gitlab.com/ce/university/glossary/README.html)
- [GitLab Positioning](https://about.gitlab.com/handbook/positioning-faq/)
- [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)
- [Sales Qualification Questions](https://about.gitlab.com/handbook/sales-qualification-questions/)
- [FAQ from Prospects](https://about.gitlab.com/handbook/sales-faq-from-prospects/)
- [Customer Use Cases](https://about.gitlab.com/handbook/use-cases/)
- [GitLab University](https://docs.gitlab.com/ce/university/)
- [Platzi GitLab Workshop](https://courses.platzi.com/classes/git-gitlab/)
- [GitLab Market and Ecosystem](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)
- [GitLab Documentation](http://docs.gitlab.com/ee/)
- [GitLab CI/CD Demo](https://about.gitlab.com/2017/03/13/ci-cd-demo/)
- [GitLab Compared to Other Tools](https://about.gitlab.com/comparison/)
- [GitLab Battlecards](https://docs.google.com/document/d/1zRIvk4CaF3FtfLfSK2iNWsG-znlh64GNeeMwrTmia_g/edit#)
- [How to Use Outreach](https://docs.google.com/document/d/1FzvGEsL6ukxFZtk7ZkMUVAAT_wW-aVblAu1yOFWiEGo/edit)
- [Outreach University](http://university.outreach.io/)
- [Version.gitlab](https://version.gitlab.com/users/sign_in)
- [Resellers Handbook](https://about.gitlab.com/handbook/resellers/)
- [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
- [Support Handbook](https://about.gitlab.com/handbook/support/)
- [GitHost](https://about.gitlab.com/gitlab-hosted/)
- [Additional Training Resources](https://drive.google.com/drive/folders/0B1W9hTDXggO1NGJwMS12R09lakE)

### FAQ<a name="faq"></a>
Don't hesitate to ping one of your colleagues with a question, or someone on the team who specializes in what you're searching for. Everyone is happy to help, and it's better to get your work done faster with your team, than being held up at a road block.

### Questions specific to:

#### Salesforce
- #sfdc-users
- Francis Aquino

#### Lead Questions or Issues
- #lead-questions
- JJ Cordz

#### GitLab
- #support
- Assigned buddy
- BDR Team Lead (Molly Young)

#### Outreach
- #outreach
- Chet Backman
- Francis Aquino

#### Resellers
- Michael Alessio

#### Partnerships
- Eliran Mesika

### What are my work hours?
The handbook says “You should have clear objectives and the freedom to work on them as you see fit. Any instructions are open to discussion. You don’t have to defend how you spend your day. We trust team members to do the right thing instead of having rigid rules”.

### Who should I connect with for my 10 1:1 onboarding calls?
You can connect with whomever you please. Check out the [Team Page](https://about.gitlab.com/team/) for ideas. Though you will be working most closely with the Sales and Marketing teams, it is encouraged you get to know people all around the organization.

### Improvements/Contributing<a name="contribute"></a>
- If you get an answer to a question someone else may benefit from, incorporate it into this handbook or add it to the FAQ document
- After meetings or process changes, feel free to update this handbook, and submit a merge request.
- Create issues for any idea (small or large that), that you want feedback on
- All issued and MRs for changes to the SDR handbook assign to Chet Backman

---

# Outbound SDR Handbook<a name="outbound"></a>

# Table of Contents
- [Your Role](#role2)
- [Outbound Onboarding](#onboarding2)
- [Compensation](#compensation2)
- [Created Opportunity Criteria](#createdopp)
- [Accepted Opportunity Criteria](#acceptedopp)

### Your Role<a name="role2"></a>

As a Outbound SDR, you will be dealing with the front end of the sales process. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating qualified opportunities, within our large and strategic account segmentation, for GitLab as you hunt for prospects, research companies, industries, and different roles.

As you gain knowledge, you will be able to aid these key players in solving problems within the developer lifecycle.


### Working with AE’s

Account Executives are provided support from the Outbound SDR team. Weekly or biweekly strategy meetings should be held and consist of the following:

Note: This is just a guideline and example

- First time kick-off meeting (1hour, discuss strategy, accounts, and schedules)

- Monthly strategy meeting (1hour, evalutate strategy and opportunities)

- Weekly/biweekly status meetings (30 minutes, initial meetings, and opportunities)

AEs have direct impact on the Outbound SDR's success. Forming a strategic relationship around the assigned accounts will play a crucial role in obtaining quota each month.

### Account Distribution

* Your Team Lead will assign you ~25-30 accounts per Quarter in salesforce from GitLab's Large and Strategic segments.  These target accounts will be provided by the RD to the Team Lead and will be the acounts the RD's have identified as the best accounts to focus on in order to meet our revenue objectives. The Team Lead or Sales & Business Development Manager will add you to the SDR Field.
* If you inherit an account that was previously worked on by another SDR team member, please send a Chatter note to Courtland, Chet, Nick, JJ, or Francis as no one else has permission to reassign an SDR on an account.
* In the event that you feel your accounts are non-workable please consult with your Team Lead. If they decide the account isn't workable, we will reassign all accounts and related contacts back to NULL, which will make them available for prospecting by other members of the SDR team.
* We use this SalesForce [report](https://na34.salesforce.com/00O61000003iZLP) to track and ensure proper coverage of stategic and large accounts and effective use of SDR resources.


### SDR Compensation<a name="compensation2"></a>

* SDR’s compensation is based on two key metrics:
   * Sales Accepted Opportunities (SAOs)
   * Closed won business from SAOs - 1% commission for any closed won opportunity produced, so as long as the rep is in seat as a SDR.

#### Accelerator

* Base rate: Per accepted opportunity commission is paid at a base rate of your monthly variable target earnings divided by your monthly target
* Accelerator from 100% to 200% achievement: Each accepted opportunity after initial target is achieved will be paid at 1.05 times the base rate
* Accelerator from 200% achievment onwards: After 200% quota achievement each opportunity after will be paid at 1.1 times the base rate
* There is no floor

* SDRs’ will also be measured on, but not tied to their compensation:

    * % of named accounts contacted
    * Number of opportunities created and initial qualifying meetings set
    * Number of emails sent
    * Number of calls made
    * Pipeline of SDR sourced opportunities
    * ACV won from opportunities SDR sources

#### Monthly Target

* Six sales accepted opportunities

### Criteria for a BDR or SDR created opportunity<a name="createdopp"></a>

* Right profile, right person
* Identified high-level need/pain.
* Initial meeting scheduled with an AE
* If BDRs or SDRs are booking meetings with the right types of companies, the right people within them, and the prospect is at least curious about addressing a potential pain point, then the BDRs and SDRs have done their jobs well.

#### Walk-through of an SDR created opportunity from a high level

* Call Tom, Director of Development who agrees to meet one week from today with an AE.
* Create an Opportunity in Salesforce assign the AE to the opportunity owner field.
* Name the Opportunity as [Company Name], [Person & Title]
* Attach all e-mail correspondence or correct call dispositions to the Opportunity
* Create a new event within that created opportunity record with the Subject defaulted to say "Initial Qualifying Meeting" and fill the description box with the Qualification Criteria for an SDR Created Opportunity and assign the event to the AE (Use IQM button at the top of the opportunity tab)
* In the [Next Steps] field, include the date and time of the meeting
* In the [Qualification Notes] section include : Name, Title, Linkedin Profile, and any Other Notes that will aid the AE for the IQM
* Submit the Opportunity for Approval - At this point the Opportunity Created is pending.
* Create a calendar invite in Google and invite both the Prospect and the AE. In the Where line the zoom meeting info will be inserted. In the description it will have the items detailing the flow of the conversation. E.g. - Mutual Introductions, Discussion on Current Dev ops process, Discussion on GitLab, Agreed next steps
* Day of the meeting happens AE accepts the opportunity following the criteria for an Accepted Opportunity. If the AE decides based on the conversation to reject the opportunity there will be a reason for rejection field for them to fill out. At the end of the IQM (initial qualifying meeting) the event will need to be dispositioned to either no show, meeting progressed, or meeting stalled

*Note* - A first no show should not be rejected. An opportunity should only be rejected if the prospect verbally declines our services. The SDRs will be accountable for rescheduling the initial Qualifying Meeting (IQM) and teeing the opportunity up again.

### Criteria for Sales Accepted Opportunity (SAO)<a name="acceptedopp"></a>

* Account is a Large or Strategic prospect
* Right person/right profile
* High level pain/need identified
* A potential solution was introduced
* The prospect has committed to a next step
* An opportunity does not immediately need to be moved into Discovery if it does not meet all the criteria to be in that stage, but can still be accepted
* If user count is determined after the initial discovery call, the Account Executive should update the Opportunity name accordingly

### Salesforce Hygiene for your opportunites

[Outbound SDR created opps](https://na34.salesforce.com/00O61000003nmhe) This is the report that the leadership uses to see all the Outbound SDR created opportunities.

* Attendance to the initial qualfiying meeting scheduled by the SDR is mandatory. The SDR should take notes during the initial qualifying meeting (IQM) to hand over to the AE's
* It will be in your best interest to sit in on as many meetings as possible with your different AE’s and at different stages in the buying process to see how the AE’s work with prospects beyond qualifying. The idea is to create a habits for success.
