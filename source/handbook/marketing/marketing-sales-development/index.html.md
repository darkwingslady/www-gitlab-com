---
layout: markdown_page
title: "Marketing & Sales Development"
---
# Welcome to the Marketing & Sales Development Handbook

The Marketing & Sales Development organization includes the Sales Development, Content Marketing, Field Marketing, Online Marketing, and Marketing Operations teams. The teams in this organiation employ a variety of marketing and sales crafts in service of our current and future customers with the belief that providing our audiences value will in turn grow GitLab's business.

## On this page
* [Marketing & Sales Development Handbooks](#handbooks)
* [Demand waterfall](#waterfall)
* [References](#references)

## Marketing & Sales Development Handbooks:  <a name="handbooks"></a>

- [Sales Development](/handbook/marketing/marketing-sales-development/sdr/)
- [Content Marketing](/handbook/marketing/marketing-sales-development/content)
- [Field Marketing](/handbook/marketing/marketing-sales-development/field-marketing/)
- [Online Marketing](/handbook/marketing/marketing-sales-development/online-marketing/)
- [Marketing Operations](/handbook/marketing/marketing-sales-development/marketing-operations/)

## Demand waterfall: <a name="waterfall"></a>

A demand waterfall is a term used to track the different milestones prospective customers go through as they learn about GitLab and interact with our sales and marketing teams. Each subsequent milestone is a subset of the previous milestone, and represents a progression from not knowing GitLab to being a customer (and fan of) of GitLab.

At the highest level of abstraction, we use the terms `lead` `opportunity` and `customer` to represent a person's progression towards becoming a customer of GitLab. Those three terms also correspond to record types in salesforce.com

Lead => Opportunity => Customer

However, there are more granular steps within the above milestones that are used to track the above process with more precision. They are tracked as follows:

| Funnel stage | Record Type | [Lead or Contact Status]| [Opportunity Stage] | Definition |
|---------------|--|----------------------|-------------------|------------|
| [Raw] | Lead or Contact | Raw | | Untouched brand new record in salesforce.com. Can be a contact record or a lead record in salesforce, but they have not yet indicated any interest in GitLab. |
| [Inquiry] | Lead or Contact | Inquiry | | Trial request, meeting @ trade show, contact request, webinar sign up, or any other indication of interest in GitLab. |
| [Marketo Qualified Lead] | Lead or Contact |  MQL | | Marketing automation tool has used a lead score to determine qualification through systematic means. The lead has not yet been qualified by a person. |
| [Accepted Lead] | Lead or Contact |  Accepted | | Actively working to get intouch with the lead/contact. |
| [Qualifying] | Lead or Contact |  Qualifying | | In 2-way conversation with lead/contact. |
| [Qualified Lead] | Opportunity |  Qualified | 0 - Pre AE Qualified | A qualified lead has been identified as someone who could buy GitLab and has expressed a need that our product can solve. The lead is converted into an opportunity and assigned to a sales person. |
| [Sales Accepted Opportunity] | Opportunity | | 0 - Pre AE Qualified | Initial qualifying meeting has been dispositioned as "accepted" by a sales person, or in the event the opportunity is created by the sales person in stage 0 - Pre AE Qualified, it is assumed the opportunity is accepted by sales without explicitly creating and dispositioning an initial qualifying meeting. |
| [Sales Qualified Opportunity] | Opportunity | | Stages 1 - 5 | Budget, authority, need, and timeline have been confirmed by a sales person, and the opportunity will be worked from [discovery to negotiating](https://about.gitlab.com/handbook/sales/#opportunity-stages) until the opportunity is won or lost. These stages are also known, collectively, as sales pipeline stages. |
| [Customer] | Opportunity | | 6-Closed Won | Customer has signed a contract to purchase GitLab. |

## References<a name="references"></a>

- [Marketing & Sales Development Roles, Process, and 2H 2017 Plan](https://docs.google.com/presentation/d/1NBsqRudh5ELNFHRJCjt5yxLNRrigv16qC0-_sWWAcpU/edit#slide=id.g23e881c565_0_136)
- [Reseller Handbook](https://about.gitlab.com/handbook/resellers/)

[Lead or Contact Status]: https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#statuses
[Opportunity Stage]: https://about.gitlab.com/handbook/sales/#opportunity-stages
[discovery to negotiating]: https://about.gitlab.com/handbook/sales/#opportunity-stages
[stages in our sales process]: https://about.gitlab.com/handbook/sales/#opportunity-stages
[Raw]: https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#statuses
[Inquiry]: https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#statuses
[Marketo Qualified Lead]: https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#statuses
[Accepted Lead]: https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#statuses
[Qualifying]: https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#statuses
[Qualified Lead]: https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#statuses
[Sales Accepted Opportunity]: https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/#acceptedopp
[Sales Qualified Opportunity]: https://about.gitlab.com/handbook/sales/#opportunity-stages
[Customer]: https://about.gitlab.com/handbook/sales/#opportunity-stages
