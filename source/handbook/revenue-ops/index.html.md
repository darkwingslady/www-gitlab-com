---
layout: markdown_page
title: "Revenue Operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Other Related Pages

- [SDR Handbook](/handbook/marketing/marketing-sales-development/sdr/)
- [Sales Operations](/handbook/sales/sales_ops/)
- [Marketing Operations](/handbook/marketing/marketing-sales-development/marketing-operations/)   
- [Customer Success](/handbook/customer-success/)


## Glossary  <a name="glossary"></a>   

| Term | Definition |
| :--- | :--- |
| Account | An organization tracked in salesforce.com. An account can be a prospect, customer, former customer, integrator, reseller, or prospective reseller |
| AM | Account Manager |
| AE | Account Executive |
| APAC | Asia-Pacific |
| BDR | Currently referred to as Inbound Sales Development Reps (I-SDR) |
| CS | Customer Success (Sales Engineer) |
| EMEA | Europe, Middle East and Asia |
| Inquiry | an Inbound request or response to an outbound effort |
| IQM | Initial Qualifying Meeting |
| MQL | Marketo Qualified Lead - an inquiry that has been qualified through systematic means (e.g. through demographic/firmographic/behaviour lead scoring) |
| NCSA | North, Central, South America |
| RD | Regional Director |
| Sales Admin | Sales Administrator (US and EMEA) |
| SAO | Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Inital Qualifying Meeting |
| SLA | Service Level Agreement |
| SCLAU | Abbreviation for SAO (Sales Accepted Opportunity) Count Large and Up |
| I-SDR | Inbound Sales Development Rep (FKA Inbound BDR) |
| O-SDR | Outbound Sales Development Rep (FKA Outbound BDR) |
| SDR-AL | SDR Accepted Lead - A lead Sales Development agrees to work until qualified in or out |
| SDR-QL | SDR Qualified Lead - A lead Sales Development has qualified, converted and assigned to Sales (Stage `0-Pending Accpetance`) |
| SQO | Sales Qualified Opportunity |
| TEDD | Technology, Engineering, Development and Design - used to estimate the maximum potential users of GitLab at a company |
| Won Opportunity | Contract signed to Purchase GitLab |
