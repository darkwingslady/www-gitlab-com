---
layout: job_page
title: "Developer"
---

## Developers Roles at GitLab

Developers at GitLab work on our product. This includes both the open source version of GitLab, the enterprise editions, and the GitLab.com service as well. They work with peers on teams dedicated to areas of the product. They work together with product managers, designers, and backend or frontend developers to solve common goals.


### Junior Developer

Junior Developers share the same requirements outlined below, but typically join with less or alternate experience in one of the key areas of Developer expertise (Ruby on Rails, Go, Git, reviewing code).


### Developer

* Write good code
* Peer review others’ code
* Ship small features independently
* Be positive and solution oriented
* Good written and verbal communication skills
* Constantly improve product quality, security, and performance


### Senior Developer

The Senior Developer role extends the [Developer](#developer) role.

* Write _great_ code
* Ship _medium_ features independently
* Generate architecture recommendations
* _Great_ written and verbal communication skills
* Code screen applicants during hiring process

***

A Senior Developer may want to pursue the [engineering management track](/jobs/engineering-management) at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

***


### Staff Developer

The Staff Developer role extends the [Senior Developer](#senior-developer) role.

* Write _exquisite_ code
* Ship _large_ features independently
* Make architecture decisions
* Implement technical and process improvements
* _Exquisite_ written and verbal communication skills
* Author technical architecture documents for epics
* Author code tests for hiring process and screen applicants
* Write public blog posts


### Distinguished Developer

The Distinguished Developer role extends the [Staff Developer](#staff-developer) role.

* Ship _large_ feature sets with team
* _Generate_ technical and process improvements
* Contribute to the sense of psychological safety on your team
* Work cross-departmentally
* Be a technical mentor for developers
* Author architecture documents for epics
* Hold team members accountable within their roles


### Engineering Fellow

The Engineering Fellow role extends the [Distinguished Developer](#distinguished-developer) role.

* Work on our hardest technical problems
* Ship _extra-large_ feature sets with team
* Help create the sense of psychological safety in the department
* Represent the company publicly at conferences
* Work directly with customers

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Qualified applicants receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with our Recruiting team
- Next, candidates will be invited to schedule an interview with either our Discussion Lead or Platform Engineering Manager
- Candidates will then be invited to schedule an interview with Director of Backend
- Candidates will then be invited to schedule an additional interview with VP of Engineering
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
