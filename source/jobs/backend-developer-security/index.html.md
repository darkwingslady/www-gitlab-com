---
layout: job_page
title: "Backend Developer (Security)"
---

This role will focus on security features and security products for GitLab. This role will specifically focus on security; if you want to work with Ruby on Rails and not security, please apply to our [Backend Developer role](https://jobs.lever.co/gitlab/5899df3c-4690-4c3a-a77d-1fe281a26d8a) instead.

This role will report to and collaborate directly with our CTO.

## Responsibilities

- Develop security products from proposal to polished end result.
- Integrating 3rd party security tools into GitLab.
- Key aspects of this role are focused on security products and features.
- The complexity of this role will increase over time.
- If you are willing to stick to working on these features for at least a year, then this role is for you.

## Requirements

- Strong Ruby developer with security expertise or proven security interest.
- Passion and interest toward security (scanning, dependencies, etc.).
- Experience in using GitLab and GitLab CI .
- Ability to work in European timezone.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Qualified applicants receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute screening call with our Recruiting team
- Next, candidates will be invited to schedule a 60 minute interview with our CTO
- Candidates will then be invited to schedule a 45 minute interview with our VP of Engineering
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/interviewing).
